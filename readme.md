# SQL!

copie e cole o conteúdo do arquivo filmes.sql nestes site:

- https://www.tutorialspoint.com/execute_sql_online.php

Usando sql, responda:

1) Quais filmes tem duração menor que 50 minutos?

2) Qual filme tem o id igual 10?

3) Quais filmes tem duração maior que uma hora?

4) Quais filmes foram feitos antes do ano em que você nasceu?

5) Qual o filme mais longo cadastrado?

6) Crie um sql para adicionar um filme e teste-o.
